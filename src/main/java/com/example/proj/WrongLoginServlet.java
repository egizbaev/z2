package com.example.proj;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

// Сервлет для перенаправления на страницу с сообщением об ошибке при неверном логине/пароле
@WebServlet(name = "wrongLoginServlet", value = "/wrong-login-servlet")
public class WrongLoginServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        // Выводим сообщение об ошибке в консоль
        System.out.println("here");

        // Устанавливаем заголовок ответа и перенаправляем на страницу с сообщением об ошибке (wrong_login.jsp)
        response.setHeader("Location", "wrong_login.jsp");
    }
}

