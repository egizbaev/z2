package com.example.proj;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "adminServlet", value = "/admin-servlet")
// Аннотация, которая указывает на то, что класс является сервлетом.
// Name - имя сервлета, value - путь, по которому он будет доступен.
public class AdminServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // Метод, который будет вызван при GET запросе на сервлет.

        // Forward request to admin.jsp
        // Получаем объект RequestDispatcher, который перенаправит запрос на admin.jsp
        RequestDispatcher dispatcher = request.getRequestDispatcher("admin.jsp");
        // Перенаправляем запрос и ответ на страницу admin.jsp
        dispatcher.forward(request, response);
    }
}
