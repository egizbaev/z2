package com.example.proj;

public enum ConnectionStatus {
    OK, // Подключение успешно
    WRONG_PASSWORD, // Неверный пароль
    NO // Ошибка подключения
}

