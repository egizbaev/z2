<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
  <title>Ошибка входа</title>
  <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/styles/style.css">
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<body>
<header>
  <h1>Парикмахерская</h1>
</header>
<main>
  <h2>Ошибка входа</h2>
  <p class="mdc-typography--body1">Неверный логин или пароль. Пожалуйста, попробуйте снова.</p>
  <a href="index.jsp" class="mdc-button mdc-button--raised">
    <span class="mdc-button__label">Вернуться к форме входа</span>
  </a>
</main>
<footer>
  <p>&copy; 2023 Парикмахерская</p>
</footer>
<script src="https://unpkg.com/material-components-web@13.0.0/dist/material-components-web.min.js"></script>
<script>
  mdc.autoInit();
</script>
</body>
</html>
